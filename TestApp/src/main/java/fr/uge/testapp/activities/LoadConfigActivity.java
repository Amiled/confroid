package fr.uge.testapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;

import fr.uge.confroidutils.main.ConfroidUtils;
import fr.uge.testapp.R;

public class LoadConfigActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_config);
    }

    public void onClickLoad(View v) {
        TextView textView = findViewById(R.id.load_textView);
        EditText configView = findViewById(R.id.load_configName);
        EditText tagView = findViewById(R.id.load_tag);
        String tag = tagView.getText().toString();
        String config = configView.getText().toString();

        if (tag.equals("")) {
            tag = "latest";
        }

        ConfroidUtils.loadConfiguration(getApplicationContext(), config, tag, x -> {
            String text;
            if (x instanceof byte[]) {
                text = Arrays.toString((byte[]) x);
            } else {
                text = String.valueOf(x);
            }
            textView.post(() -> textView.setText(text));
        });
    }
}