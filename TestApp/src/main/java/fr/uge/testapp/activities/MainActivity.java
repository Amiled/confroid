package fr.uge.testapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;

import fr.uge.confroidutils.main.ConfroidUtils;
import fr.uge.confroidutils.main.IllegalConfigurationException;
import fr.uge.confroidutils.main.Version;
import fr.uge.testapp.R;
import fr.uge.testapp.testclasses.BillingDetail;
import fr.uge.testapp.testclasses.ShippingAddress;
import fr.uge.testapp.testclasses.ShoppingInfo;
import fr.uge.testapp.testclasses.ShoppingPreferences;
import fr.uge.testapp.testclasses.WorldAddresses;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "pre token ensure");
        ConfroidUtils.ensureTokenPresence(getApplicationContext(), this::updateDisplay);
        Log.d(TAG, "post token ensure");
    }

    public void onClickMap(View v) {
        ShoppingPreferences prefs = new ShoppingPreferences();
        ShippingAddress address1 = new ShippingAddress("Bugdroid", "Bd Descartes", "Champs-sur-Marne", "France");
        ShippingAddress address2 = new ShippingAddress("Bugdroid", "Rue des tartes au nougat", "Lollipop City", "Oreo Country");
        BillingDetail billingDetail = new BillingDetail("Bugdroid", "123456789", 12, 2021, 123);
        prefs.shoppingInfos.put("home", new ShoppingInfo(address1, billingDetail, true));
        prefs.shoppingInfos.put("work", new ShoppingInfo(address2, billingDetail, false));

        try {
            ConfroidUtils.saveConfiguration(getApplicationContext(), "shoppingPreferences", prefs, "stable");
        } catch (IllegalConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void onClickList(View v) {
        WorldAddresses worldAddresses = new WorldAddresses();
        worldAddresses.addresses.add(new ShippingAddress("Broken Glass", "Domination Route", "Red Mold","USA"));
        worldAddresses.addresses.add(new ShippingAddress("Forbidden Fruit", "Rich Avenue", "Glossy City","Challenger Kingdom"));

        try {
            ConfroidUtils.saveConfiguration(getApplicationContext(), "shippingAddresses", worldAddresses, "september");
        } catch (IllegalConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void onClickPrimitive(View v) {
        Intent intent = new Intent(this, SendPrimitiveActivity.class);
        startActivity(intent);
    }

    public void onClickPartial(View v) {
        onClickMap(v);

        ShippingAddress address2 = new ShippingAddress("Bugdroid", "Rue des tartes au nougat", "Lollipop City", "Oreo Country");
        BillingDetail billingDetail = new BillingDetail("Bugdroid", "987654321", 5, 2025, 987);
        ShoppingInfo info = new ShoppingInfo(address2, billingDetail, false);

        try {
            ConfroidUtils.saveConfiguration(getApplicationContext(), "shoppingPreferences/shoppingInfos.work", info, null);
        } catch (IllegalConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void onClickBytes(View v) {
        byte[] bytes = {-128, 0, 127, 1, -98, 40, 2, 5, 3};

        try {
            ConfroidUtils.saveConfiguration(getApplicationContext(), "superSecretKey", bytes, "five eyes");
        } catch (IllegalConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void onClickLoad(View v) {
        Intent intent = new Intent(this, LoadConfigActivity.class);
        startActivity(intent);
    }

    public void onClickVersions(View v) {
        ConfroidUtils.getConfigurationVersions(getApplicationContext(), "shoppingPreferences", versions -> {
            for (Version version: versions) {
                Log.d(TAG, version.toString());
            }
        });
    }

    public void onClickRequestToken(View v) {
        ConfroidUtils.ensureTokenPresence(getApplicationContext(), this::updateDisplay);
    }

    public void onClickRevokeToken(View v) {
        ConfroidUtils.ensureTokenAbsence(getApplicationContext(), this::updateDisplay);
    }

    private void updateDisplay() {
        String token = ConfroidUtils.getLocalToken(getApplicationContext());
        TextView textView = findViewById(R.id.tokenTextView);

        if (token != null) {
            textView.setText(String.format(getString(R.string.token_formatter), token));
            textView.setVisibility(View.VISIBLE);

            findViewById(R.id.buttonMap).setEnabled(true);
            findViewById(R.id.buttonList).setEnabled(true);
            findViewById(R.id.buttonPrimitive).setEnabled(true);
            findViewById(R.id.buttonBytes).setEnabled(true);
            findViewById(R.id.buttonPartial).setEnabled(true);
            findViewById(R.id.buttonLoad).setEnabled(true);
            findViewById(R.id.buttonVersions).setEnabled(true);
            findViewById(R.id.buttonTokenRequest).setEnabled(false);
            findViewById(R.id.buttonTokenRevoke).setEnabled(true);
        } else {
            textView.setText(null);
            textView.setVisibility(View.GONE);

            findViewById(R.id.buttonMap).setEnabled(false);
            findViewById(R.id.buttonList).setEnabled(false);
            findViewById(R.id.buttonPrimitive).setEnabled(false);
            findViewById(R.id.buttonBytes).setEnabled(false);
            findViewById(R.id.buttonPartial).setEnabled(false);
            findViewById(R.id.buttonLoad).setEnabled(false);
            findViewById(R.id.buttonVersions).setEnabled(false);
            findViewById(R.id.buttonTokenRequest).setEnabled(true);
            findViewById(R.id.buttonTokenRevoke).setEnabled(false);
        }
    }

}