package fr.uge.testapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.uge.confroidutils.main.ConfroidUtils;
import fr.uge.confroidutils.main.IllegalConfigurationException;
import fr.uge.testapp.R;

public class SendPrimitiveActivity extends AppCompatActivity {
    private static final String TAG = "SendPrimitiveActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_primitive);
    }

    public void onClickSend(View v) {
        EditText configView = findViewById(R.id.save_configName);
        EditText tagView = findViewById(R.id.save_tag);
        EditText valueView = findViewById(R.id.save_value);

        String config = configView.getText().toString();
        String tag = tagView.getText().toString();
        if (tag.equals("")) {
            tag = null;
        }
        String value = valueView.getText().toString();

        try {
            ConfroidUtils.saveConfiguration(getApplicationContext(), config, value, tag);
        } catch (IllegalConfigurationException | IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), "This configuration is invalid!", Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}