package fr.uge.testapp.testclasses;

import java.util.HashMap;
import java.util.Map;

public class ShoppingPreferences {
    public Map<String, ShoppingInfo> shoppingInfos;

    public ShoppingPreferences() {
        shoppingInfos = new HashMap<>();
    }

    @Override
    public String toString() {
        return "ShoppingPreferences{" +
                "shoppingInfos=" + shoppingInfos +
                '}';
    }
}