package fr.uge.testapp.testclasses;

public class ShoppingInfo {
    public ShippingAddress address;
    public BillingDetail billing;
    public boolean favorite;

    public ShoppingInfo() {}

    public ShoppingInfo(ShippingAddress address, BillingDetail billing, boolean favorite) {
        this.address = address;
        this.billing = billing;
        this.favorite = favorite;
    }

    @Override
    public String toString() {
        return "ShoppingInfo{" +
                "address=" + address +
                ", billing=" + billing +
                ", favorite=" + favorite +
                '}';
    }
}