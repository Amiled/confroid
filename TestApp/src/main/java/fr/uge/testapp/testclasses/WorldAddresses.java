package fr.uge.testapp.testclasses;

import java.util.ArrayList;
import java.util.List;

public class WorldAddresses {
    public List<ShippingAddress> addresses;

    public WorldAddresses() {
        addresses = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "WorldAddresses{" +
                "addresses=" + addresses +
                '}';
    }
}
