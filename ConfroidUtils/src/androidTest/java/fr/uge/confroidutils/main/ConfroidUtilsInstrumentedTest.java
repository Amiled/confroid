package fr.uge.confroidutils.main;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ConfroidUtilsInstrumentedTest {
    private static class Card {
        public String name;
        public Map<String, String> details = new HashMap<>();

        public Card(String name) {
            this.name = name;
        }
    }

    private static class Deck {
        public List<Card> cards = new ArrayList<>();
    }

    @Test
    public void shouldThrowIllegalConfigurationException() {
        Deck deck = new Deck();
        Card card = new Card("Daniel Elena");
        card.details.put("type", "Copilote");
        card.details.put("class", "En colère");
        deck.cards.add(card);

        try {
            ObjectToBundle.valueToBundle(deck);
            fail("Should have thrown an exception");
        } catch (IllegalConfigurationException ignored) {
            // pass test
        }
    }
}