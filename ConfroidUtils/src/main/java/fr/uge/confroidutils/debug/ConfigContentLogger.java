package fr.uge.confroidutils.debug;

import android.os.Bundle;
import android.util.Log;

import java.util.Arrays;

public class ConfigContentLogger {
    protected static void log(Bundle bundle) {
        log(bundle, 0);
    }

    private static String indent(int level) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < level; i++) {
            s.append("  ");
        }
        return s.toString();
    }

    private static void log(Bundle bundle, int indentLevel) {
        if (bundle.containsKey("class")) {
            Log.i("Bundle Log", indent(indentLevel) + "class:" + bundle.getString("class"));
        }

        for (String key: bundle.keySet()) {
            if (key.equals("class")) {
                continue;
            }
            Object value = bundle.get(key);
            Class<?> cls = value.getClass();
            if (cls == Bundle.class) {
                Log.i("Bundle Log", indent(indentLevel) + key + ':');
                log(bundle.getBundle(key), indentLevel + 1);
            } else if (cls == String.class) {
                Log.i("Bundle Log", indent(indentLevel + 1) + key + ':' + bundle.getString(key));
            } else if (cls == byte[].class) {
                Log.i("Bundle Log", indent(indentLevel + 1) + key + ':' + Arrays.toString(bundle.getByteArray(key)));
            } else if (cls == Float.class) {
                Log.i("Bundle Log", indent(indentLevel + 1) + key + ':' + bundle.getFloat(key));
            } else if (cls == Integer.class) {
                Log.i("Bundle Log", indent(indentLevel + 1) + key + ':' + bundle.getInt(key));
            } else if (cls == Boolean.class) {
                Log.i("Bundle Log", indent(indentLevel + 1) + key + ':' + bundle.getBoolean(key));
            }
        }
    }
}
