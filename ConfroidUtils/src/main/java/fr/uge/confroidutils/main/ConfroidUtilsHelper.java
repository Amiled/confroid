package fr.uge.confroidutils.main;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

public class ConfroidUtilsHelper {
    static String prefixedConfigName(Context context, String name) {
        return context.getPackageName() + " " + name;
    }

    static void saveConfigurationChecks(Context context, String name, Object value, String versionName) {
        Objects.requireNonNull(context, "Context must not be null");
        nameCheck(name);
        if (name.indexOf("/") != name.lastIndexOf("/")) {
            throw new IllegalArgumentException("Partial configuration name contains too much slashes");
        }
        if (value == null && versionName == null) {
            throw new IllegalArgumentException("value and versionName must not be null at the same time");
        }
        if ("".equals(versionName)) {
            throw new IllegalArgumentException("Version name must not be empty if it is defined");
        }
        if ("latest".equals(versionName)) {
            throw new IllegalArgumentException("Version name must not be \"latest\"");
        }
        tokenCheck(context);
    }

    static <T> void loadConfigurationChecks(Context context, String name, String versionName, Consumer<T> callback) {
        Objects.requireNonNull(context, "Context must not be null");
        nameCheck(name);
        versionNameCheck(versionName);
        Objects.requireNonNull(callback, "Callback must not be null");
        tokenCheck(context);
    }

    static void getConfigurationVersionsChecks(Context context, String name, Consumer<List<Version>> callback) {
        Objects.requireNonNull(context);
        nameCheck(name);
        Objects.requireNonNull(callback);
        tokenCheck(context);
    }

    private static void nameCheck(String name) {
        Objects.requireNonNull(name, "Configuration name must not be null");
        if (name.equals("")) {
            throw new IllegalArgumentException("Configuration name must not be empty");
        }
        if (name.endsWith("/")) {
            throw new IllegalArgumentException("Configuration name must not end with a slash");
        }
    }

    private static void versionNameCheck(String name) {
        Objects.requireNonNull(name, "Version name must not be null");
        if (name.equals("")) {
            throw new IllegalArgumentException("Version name must not be empty");
        }
    }

    private static void tokenCheck(Context context) {
        if (!ConfroidUtils.hasToken(context)) {
            throw new IllegalStateException("No Confroid token!");
        }
    }
}
