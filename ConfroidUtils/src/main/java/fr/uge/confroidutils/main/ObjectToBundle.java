package fr.uge.confroidutils.main;

import android.os.Bundle;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class ObjectToBundle {
    public static Bundle valueToBundle(Object value) throws IllegalConfigurationException {
        Bundle bundle = new Bundle();
        Class <?> containerClass = value.getClass();
        bundle.putString("class", containerClass.getName());

        for (Field field: containerClass.getFields()) {
            try {
                Object object = field.get(value);
                if (object == null) {
                    throw new IllegalConfigurationException("Configuration contains null values");
                }
                putElem(bundle, field.getName(), object);
            } catch (IllegalAccessException e) {
                throw new IllegalConfigurationException(containerClass, field);
            }
        }
        return bundle;
    }

    public static boolean tryPutPrimitive(Bundle bundle, String field, Object value) {
        Class<?> cls = value.getClass();
        if (cls.equals(String.class)) {
            bundle.putString(field, (String) value);
            return true;
        } else if (cls.equals(byte[].class)) {
            bundle.putByteArray(field, (byte[]) value);
            return true;
        } else if (cls.equals(Float.class)) {
            bundle.putFloat(field, (float) value);
            return true;
        } else if (cls.equals(Integer.class)) {
            bundle.putInt(field, (int) value);
            return true;
        } else if (cls.equals(Boolean.class)) {
            bundle.putBoolean(field, (boolean) value);
            return true;
        }
        return false;
    }

    private static void putElem(Bundle bundle, String field, Object value) throws IllegalConfigurationException {
        if (value == null) {
            throw new IllegalConfigurationException("Configuration contains null values");
        }

        if (!tryPutPrimitive(bundle, field, value)) {
            if (value instanceof List) {
                bundle.putBundle(field, listToBundle((List<?>) value));
            } else if (value instanceof Map) {
                bundle.putBundle(field, mapToBundle((Map<?, ?>) value));
            } else {
                bundle.putBundle(field, valueToBundle(value));
            }
        }
    }

    private static Bundle listToBundle(List<?> list) throws IllegalConfigurationException {
        Bundle bundle = new Bundle();
        for (int i = 0; i < list.size(); i++) {
            Object value = list.get(i);
            putElem(bundle, Integer.toString(i), value);
        }
        return bundle;
    }

    private static Bundle mapToBundle(Map<?,?> map) throws IllegalConfigurationException {
        if (map.containsKey("class")) {
            throw new IllegalConfigurationException("Your maps must not contain the key \"class\"");
        }
        Bundle bundle = new Bundle();
        for (Object key : map.keySet()) {
            Object value = map.get(key);
            putElem(bundle, key.toString(), value);
        }
        return bundle;
    }
}
