package fr.uge.confroidutils.main;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class TokenTask extends AsyncTask<Context, Void, Void> {
    private static final String TAG = "TokenTask";

    private final Consumer<Context> consumer;
    private final Predicate<Context> predicate;
    private final Runnable runnable;
    private final Object lock = new Object();

    private TokenTask(Consumer<Context> consumer, Predicate<Context> predicate, Runnable runnable) {
        this.consumer = consumer;
        this.predicate = predicate;
        this.runnable = runnable;
    }

    static void addToken(Context context, Runnable callback) {
        new TokenTask(ConfroidUtils::newToken, ConfroidUtils::hasToken, callback).execute(context);
    }

    static void delToken(Context context, Runnable callback) {
        new TokenTask(ConfroidUtils::revokeToken, c -> !ConfroidUtils.hasToken(c), callback).execute(context);
    }

    @Override
    protected Void doInBackground(Context... contexts) {
        if (predicate.test(contexts[0])) {
            return null;
        }

        synchronized (lock) {
            consumer.accept(contexts[0]);
            while (!predicate.test(contexts[0])) {
                try {
                    Log.d(TAG, "Waiting for response from Confroid...");
                    lock.wait(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    protected void onPostExecute(Void result) {
        if (runnable != null) {
            runnable.run();
        }
    }
}
