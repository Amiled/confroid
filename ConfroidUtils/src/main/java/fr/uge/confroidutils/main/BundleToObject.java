package fr.uge.confroidutils.main;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BundleToObject {
    private static final String TAG = "BundleToObject";

    static Object bundleToObject(@Nullable Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        try {
            if (bundle.containsKey("class")) {
                return bundleToCustomClass(bundle);
            } else if (isListBundle(bundle)) {
                return bundleToList(bundle);
            } else {
                return bundleToMap(bundle);
            }
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            return null;
        }
    }

    private static boolean isListBundle(Bundle bundle) {
        for (String key: bundle.keySet()) {
            try {
               Integer.parseInt(key);
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return true;
    }

    private static List<?> bundleToList(Bundle bundle) throws ClassNotFoundException, NoSuchFieldException, InstantiationException, IllegalAccessException, InvocationTargetException {
        int size = bundle.keySet().size();
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            String index = Integer.toString(i);
            list.add(getItem(bundle, index));
        }
        return list;
    }

    private static Map<?, ?> bundleToMap(Bundle bundle) throws ClassNotFoundException, NoSuchFieldException, InstantiationException, IllegalAccessException, InvocationTargetException {
        Map<Object, Object> map = new HashMap<>();
        for (String key: bundle.keySet()) {
            map.put(key, getItem(bundle, key));
        }
        return map;
    }

    private static Object bundleToCustomClass(Bundle bundle) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, InstantiationException {
        String className = bundle.getString("class");
        Log.d(TAG, className);
        Class<?> containerClass = getClassObject(className);
        Object instance = getInstance(containerClass);

        for (String key: bundle.keySet()) {
            if (key.equals("class")) {
                continue;
            }
            Field field = getField(containerClass, key);
            setField(instance, field, getItem(bundle, key));
        }
        return instance;
    }

    private static Object getItem(Bundle bundle, String key) throws ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        Object item = bundle.get(key);
        if (item == null) {
            return null;
        }
        Class<?> cls = item.getClass();
        if (cls.equals(Bundle.class)) {
            return bundleToObject((Bundle) item);
        } if (cls.equals(String.class)) {
            return bundle.getString(key);
        } if (cls.equals(byte[].class)) {
            return bundle.getByteArray(key);
        } if (cls.equals(Float.class) || cls.equals(float.class)) {
            return bundle.getFloat(key);
        } if (cls.equals(Integer.class) || cls.equals(int.class)) {
            return bundle.getInt(key);
        } if (cls.equals(Boolean.class) || cls.equals(boolean.class)) {
            return bundle.getBoolean(key);
        }
        return bundleToCustomClass(bundle.getBundle(key));
    }

    private static Class<?> getClassObject(String name) throws ClassNotFoundException {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            Log.d(TAG, "Class " + name + "couldn't be found; You probably renamed or deleted it");
            throw e;
        }
    }

    private static Field getField(Class<?> cls, String key) throws NoSuchFieldException {
        try {
            return cls.getField(key);
        } catch (NoSuchFieldException e) {
            Log.d(TAG, "Field " + key + " of class " + cls.getCanonicalName()
                    + "couldn't be found; You probably renamed or deleted it");
            throw e;
        }
    }

    private static Object getInstance(Class<?> cls) throws InstantiationException, InvocationTargetException, IllegalAccessException {
        try {
            return cls.getConstructors()[0].newInstance();
        } catch (InstantiationException e) {
            Log.d(TAG, "Your configuration should only contain types supported by Confroid");
            throw e;
        } catch (InvocationTargetException e) {
            Log.d(TAG, "Your constructor shouldn't throw exceptions");
            throw e;
        } catch (IllegalAccessException e) {
            Log.d(TAG, "Your constructor must be public");
            throw e;
        }
    }

    private static void setField(Object instance, Field field, Object value) throws IllegalAccessException {
        try {
            field.set(instance, value);
        } catch (IllegalAccessException e) {
            Log.d(TAG, "Field " + field.getName() + " of class " + field.getDeclaringClass().getCanonicalName() + "must be public");
            throw e;
        }
    }
}
