package fr.uge.confroidutils.main;

import java.lang.reflect.Field;

public class IllegalConfigurationException extends Exception {
    IllegalConfigurationException(Class<?> cls, Field field) {
        super("Field " + field + " of class " + cls + "is not accessible");
    }

    public IllegalConfigurationException(String message) {
        super(message);
    }
}
