package fr.uge.confroidutils.main;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import javax.security.auth.callback.Callback;

import fr.uge.confroidutils.BuildConfig;


public class ReceiverService extends IntentService {
    private static final String TAG = "ReceiverService";

    private static final int NOTIFICATION_ID = 1000;
    private static final String NOTIFICATION_CHANNEL_ID = BuildConfig.LIBRARY_PACKAGE_NAME + ".SERVICE_CHANNEL_ID";

    private static final String ACTION_TOKEN_RECEIVER = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.action.TOKEN_RECEIVER";
    private static final String ACTION_TOKEN_DELETER = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.action.TOKEN_DELETER";
    private static final String ACTION_VERSIONS_RECEIVER = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.action.VERSIONS_RECEIVER";
    private static final String ACTION_CONFIG_RECEIVER = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.action.CONFIG_RECEIVER";

    private static final String EXTRA_TOKEN = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.extra.TOKEN";
    private static final String EXTRA_VERSIONS = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.extra.VERSIONS";
    private static final String EXTRA_REQUEST_ID = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.extra.REQUEST_ID";
    private static final String EXTRA_CONFIG_NAME = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.extra.CONFIG_NAME";
    private static final String EXTRA_VERSION = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.extra.VERSION";
    private static final String EXTRA_CONTENT = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.extra.CONTENT";
    private static final String EXTRA_CONTENT_TYPE = BuildConfig.LIBRARY_PACKAGE_NAME + ".services.extra.CONTENT_TYPE";

    private static final Map<Long, Consumer<List<Version>>> versionMap = new ConcurrentHashMap<>();
    private static final Map<Long, Consumer<Object>> loadMap = new ConcurrentHashMap<>();

    public ReceiverService() {
        super("TokenReceiverService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            displayNotification();
        }
        new Thread(() -> {
            String action = intent.getAction();
            switch (action) {
                case ACTION_TOKEN_RECEIVER: handleTokenReceiver(intent); break;
                case ACTION_TOKEN_DELETER: handleTokenDeleter(intent); break;
                case ACTION_VERSIONS_RECEIVER: handleVersionReceiver(intent); break;
                case ACTION_CONFIG_RECEIVER: handleConfigReceiver(intent); break;
                default: Log.d(TAG, "Unknown action: " + action);
            }
        }).start();
    }

    private void handleTokenReceiver(Intent intent) {
        String token = intent.getStringExtra(EXTRA_TOKEN);

        Log.d(TAG, ACTION_TOKEN_RECEIVER);
        Log.d(TAG, token);

        ConfroidUtils.setLocalToken(getApplicationContext(), token);
    }

    private void handleTokenDeleter(Intent intent) {
        String localToken = ConfroidUtils.getLocalToken(getApplicationContext());
        String received = intent.getStringExtra(EXTRA_TOKEN);

        Log.d(TAG, ACTION_TOKEN_DELETER);
        Log.d(TAG, received);

        if (!localToken.equals(received)) {
            Log.d(TAG, "Received a intent from a different application than Confroid");
            Log.d(TAG, "expected: " + localToken);
            Log.d(TAG, "received: " + received);
            stopSelf();
        }

        ConfroidUtils.delLocalToken(getApplicationContext());
    }

    private void handleVersionReceiver(Intent intent) {
        long requestId = intent.getLongExtra(EXTRA_REQUEST_ID, -1);
        Bundle versions = intent.getBundleExtra(EXTRA_VERSIONS);

        Log.d(TAG, ACTION_TOKEN_RECEIVER);
        Log.d(TAG, String.valueOf(versionMap.get(requestId)));
        Log.d(TAG, String.valueOf(versions));

        Consumer<List<Version>> consumer = versionMap.remove(requestId);
        if (consumer != null) {
            List<Version> list = new ArrayList<>();
            for (String key: versions.keySet()) {
                Bundle bundle = versions.getBundle(key);
                list.add(new Version(Integer.parseInt(key), bundle.getLong("timestamp"), bundle.getString("tag")));
            }
            consumer.accept(list);
        }
    }

    private void handleConfigReceiver(Intent intent) {
        long requestId = intent.getLongExtra(EXTRA_REQUEST_ID, -1);
        String configName = intent.getStringExtra(EXTRA_CONFIG_NAME);

        Log.d(TAG, ACTION_CONFIG_RECEIVER);
        Log.d(TAG, requestId + "");
        Log.d(TAG, "configName: " + configName);

        Consumer<Object> consumer = loadMap.remove(requestId);

        if (intent.hasExtra(EXTRA_VERSION) && intent.hasExtra(EXTRA_CONTENT) && intent.hasExtra(EXTRA_CONTENT_TYPE)) {
            int version = intent.getIntExtra(EXTRA_VERSION, -1);
            String type = intent.getStringExtra(EXTRA_CONTENT_TYPE);
            Object content = null;

            if ("bundle".equals(type)) {
                content = BundleToObject.bundleToObject(intent.getBundleExtra(EXTRA_CONTENT));
            } else if ("string".equals(type)) {
                content = intent.getStringExtra(EXTRA_CONTENT);
            } else if ("bytes".equals(type)) {
                content = intent.getByteArrayExtra(EXTRA_CONTENT);
            } else if ("float".equals(type)) {
                content = intent.getFloatExtra(EXTRA_CONTENT, 0);
            } else if ("integer".equals(type)) {
                content = intent.getIntExtra(EXTRA_CONTENT, 0);
            } else if ("boolean".equals(type)) {
                content = intent.getBooleanExtra(EXTRA_CONTENT, false);
            }
            consumer.accept(content);
        } else {
            consumer.accept(null);
        }
    }

    static void putVersionCallback(long requestId, Consumer<List<Version>> callback) {
        versionMap.put(requestId, callback);
    }

    @SuppressWarnings("unchecked")
    static <T> void putLoadCallback(long requestId, Consumer<T> callback) {
        loadMap.put(requestId, (Consumer<Object>) callback);
    }

    private void displayNotification() {
        createNotificationChannel();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                //.setSmallIcon()
                .setContentTitle("ConfroidUtils")
                .setContentText("Interracting with Confroid...")
                .setPriority(NotificationCompat.PRIORITY_LOW);

        startForeground(NOTIFICATION_ID, builder.build());
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "ConfroidUtils";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}