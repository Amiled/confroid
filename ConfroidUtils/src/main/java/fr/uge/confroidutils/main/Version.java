package fr.uge.confroidutils.main;


import java.util.Optional;

public class Version {
    private final int number;
    private final long timestamp;
    private final String tag;

    public Version(int number, long timestamp, String tag) {
        this.number = number;
        this.timestamp = timestamp;
        this.tag = tag;
    }

    public int getNumber() {
        return number;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Optional<String> getTag() {
        return Optional.ofNullable(tag);
    }

    @Override
    public String toString() {
        return "Version{" +
                "number=" + number +
                ", timestamp=" + timestamp +
                ", tag='" + tag + '\'' +
                '}';
    }
}
