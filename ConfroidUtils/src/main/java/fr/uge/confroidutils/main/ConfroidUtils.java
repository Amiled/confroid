package fr.uge.confroidutils.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Consumer;

import static fr.uge.confroidutils.main.ConfroidUtilsHelper.getConfigurationVersionsChecks;
import static fr.uge.confroidutils.main.ConfroidUtilsHelper.loadConfigurationChecks;
import static fr.uge.confroidutils.main.ConfroidUtilsHelper.saveConfigurationChecks;


public class ConfroidUtils {
    private static final String TAG = "ConfroidUtils";

    private static final String CONFROID_PACKAGE_NAME = "fr.uge.confroid";
    private static final String CONFROID_PUSHER_CLASS_NAME = CONFROID_PACKAGE_NAME + ".services.ConfigurationPusher";
    private static final String CONFROID_SERVICE_CLASS_NAME = CONFROID_PACKAGE_NAME + ".services.MainService";

    private static final String ACTION_TOKEN_DISPENSER = CONFROID_PACKAGE_NAME + ".services.action.TOKEN_DISPENSER";
    private static final String ACTION_TOKEN_REVOKER = CONFROID_PACKAGE_NAME + ".services.action.TOKEN_REVOKER";
    private static final String ACTION_VERSIONS = CONFROID_PACKAGE_NAME + ".services.action.VERSIONS";
    private static final String ACTION_PULL_CONFIGURATION = CONFROID_PACKAGE_NAME + ".services.action.PULL_CONFIGURATION";

    private static final String EXTRA_PACKAGE_NAME = CONFROID_PACKAGE_NAME + ".services.extra.PACKAGE_NAME";
    private static final String EXTRA_RECEIVER = CONFROID_PACKAGE_NAME + ".services.extra.RECEIVER";
    private static final String EXTRA_CONFIG = CONFROID_PACKAGE_NAME + ".services.extra.CONFIG";
    private static final String EXTRA_TOKEN = CONFROID_PACKAGE_NAME + ".services.extra.TOKEN";
    private static final String EXTRA_CONFIG_NAME = CONFROID_PACKAGE_NAME + ".services.extra.CONFIG_NAME";
    private static final String EXTRA_REQUEST_ID = CONFROID_PACKAGE_NAME + ".services.extra.REQUEST_ID";
    private static final String EXTRA_TAG = CONFROID_PACKAGE_NAME + ".services.extra.TAG";
    // TODO: expiration
    // private static final String EXTRA_EXPIRATION = CONFROID_PACKAGE_NAME + ".services.extra.EXPIRATION";

    private static final String CONFROID_PREFERENCES_FILE = "Confroid";
    private static final String CONFROID_PREFERENCES_TOKEN = "token";


    /**Sends the configuration represented by {@code value} to Confroid.
     *
     * @throws NullPointerException if config is null
     * @throws IllegalConfigurationException if the configuration contains null objects or unaccessible fields
     */
    public static void saveConfiguration(Context context, String name, Object value, String versionName) throws IllegalConfigurationException {
        saveConfigurationChecks(context, name, value, versionName);

        Bundle config = new Bundle();
        config.putString("name", ConfroidUtilsHelper.prefixedConfigName(context, name));
        config.putString("token", getLocalToken(context));
        config.putLong("timestamp", System.currentTimeMillis());
        if (value != null) {
            if (!ObjectToBundle.tryPutPrimitive(config, "content", value)) {
                if (value instanceof List || value instanceof Map) {
                    throw new IllegalConfigurationException("Please wrap your configuration into a custom object");
                } else {
                    Bundle content = ObjectToBundle.valueToBundle(value);
                    config.putBundle("content", content);
                }
            }
        }

        if (versionName != null) {
            config.putString("tag", versionName);
        }

        Intent intent = new Intent();
        intent.setClassName(CONFROID_PACKAGE_NAME, CONFROID_PUSHER_CLASS_NAME);
        intent.putExtra(EXTRA_CONFIG, config);
        ContextCompat.startForegroundService(context, intent);
    }

    /**Loads a configuration represented by {@code value} from Confroid.
     *
     * @throws NullPointerException if {@code context}, {@code name}, {@code versionName} or {@code callback} are null
     */
    public static <T> void loadConfiguration(Context context, String name, String versionName, Consumer<T> callback) {
        loadConfigurationChecks(context, name, versionName, callback);

        long requestId = getRequestId();
        ReceiverService.putLoadCallback(requestId, callback);

        Intent service = new Intent();
        service.setClassName(CONFROID_PACKAGE_NAME, CONFROID_SERVICE_CLASS_NAME);
        service.setAction(ACTION_PULL_CONFIGURATION);
        service.putExtra(EXTRA_CONFIG_NAME, ConfroidUtilsHelper.prefixedConfigName(context, name));
        service.putExtra(EXTRA_TOKEN, getLocalToken(context));
        service.putExtra(EXTRA_REQUEST_ID, requestId);
        service.putExtra(EXTRA_TAG, versionName);
        service.putExtra(EXTRA_RECEIVER, ReceiverService.class.getCanonicalName());
        // TODO: expiration
        // service.putExtra(EXTRA_EXPIRATION, 0);
        ContextCompat.startForegroundService(context, service);
        // TODO
    }

    public static <T> void subscribeConfiguration(Context context, String name, Consumer<T> callback) {
        // TODO
    }

    public static <T> void cancelConfigurationSubscription(Context context, Consumer<T> callback) {
        // TODO
    }

    /**
     * Retrieves all versions of the configuration {@code name}.
     *
     * @throws NullPointerException if {@code context}, {@code name}, {@code callback} are null,
     * or if {@code name] is the empty string
     */
    public static void getConfigurationVersions(Context context, String name, Consumer<List<Version>> callback) {
        getConfigurationVersionsChecks(context, name, callback);

        long requestId = getRequestId();
        ReceiverService.putVersionCallback(requestId, callback);

        Intent service = new Intent();
        service.setClassName(CONFROID_PACKAGE_NAME, CONFROID_SERVICE_CLASS_NAME);
        service.setAction(ACTION_VERSIONS);
        service.putExtra(EXTRA_CONFIG_NAME, ConfroidUtilsHelper.prefixedConfigName(context, name));
        service.putExtra(EXTRA_TOKEN, getLocalToken(context));
        service.putExtra(EXTRA_REQUEST_ID, requestId);
        service.putExtra(EXTRA_RECEIVER, ReceiverService.class.getCanonicalName());
        ContextCompat.startForegroundService(context, service);
    }

    private static long getRequestId() {
        return Math.abs(new Random().nextLong());
    }

    //<T> void editObject(Context context, T originalObject, Consumer<T> callback)
    //<T> void updateObjet(Context context, String name, String version, Consumer<T> callback)

    public static void ensureTokenPresence(Context context, Runnable callback) {
        TokenTask.addToken(context, callback);
    }

    /**
     * Revoke the token currently owned by the app, if it has one.
     * <br>
     * Note: all configurations created with this token will be deleted.
     */
    public static void ensureTokenAbsence(Context context, Runnable callback) {
        TokenTask.delToken(context, callback);
    }

    /**
    * Request a new token from Confroid.
    */
    static void newToken(Context context) {
        Intent intent = new Intent();
        intent.setClassName(CONFROID_PACKAGE_NAME, CONFROID_SERVICE_CLASS_NAME);
        intent.setAction(ACTION_TOKEN_DISPENSER);
        intent.putExtra(EXTRA_RECEIVER, ReceiverService.class.getCanonicalName());
        intent.putExtra(EXTRA_PACKAGE_NAME, context.getPackageName());
        ContextCompat.startForegroundService(context, intent);
    }

    /**
     * Delete this token's configurations from Confroid.
     */
    static void revokeToken(Context context) {
        String token = getLocalToken(context);
        Intent intent = new Intent();
        intent.setClassName(CONFROID_PACKAGE_NAME, CONFROID_SERVICE_CLASS_NAME);
        intent.setAction(ACTION_TOKEN_REVOKER);
        intent.putExtra(EXTRA_TOKEN, token);
        intent.putExtra(EXTRA_RECEIVER, ReceiverService.class.getCanonicalName());
        intent.putExtra(EXTRA_PACKAGE_NAME, context.getPackageName());
        ContextCompat.startForegroundService(context, intent);
    }

    /**
     * Retrieves this app's locally stored token, or null if the app doesn't have one.
     */
    public static String getLocalToken(Context context) {
        return getLocalToken(getPrefs(context));
    }

    private static String getLocalToken(SharedPreferences prefs) {
        return prefs.getString(CONFROID_PREFERENCES_TOKEN, null);
    }

    /**
     * Set this app's locally stored token.
     */
    public static void setLocalToken(Context context, String token) {
        getPrefs(context).edit().putString(CONFROID_PREFERENCES_TOKEN, token).apply();
    }

    /**
     * Delete this app's locally stored token.
     */
    static void delLocalToken(Context context) {
        getPrefs(context).edit().remove(CONFROID_PREFERENCES_TOKEN).apply();
    }

    static boolean hasToken(Context context) {
        return hasToken(getPrefs(context));
    }

    private static boolean hasToken(SharedPreferences prefs) {
        return prefs.contains(CONFROID_PREFERENCES_TOKEN);
    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(CONFROID_PREFERENCES_FILE, Context.MODE_PRIVATE);
    }
}