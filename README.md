# Projet Android 


## Introduction du projet : 
 
Le projet Confroid consiste à réaliser une application et bibliothèque de gestion de configurations d'application centralisée. Elle permettra à d'autres applications de lui envoyer ses configurations et les modifier. Les configurations sont stockées et affichées selon un mode transactionnel avec un historique de versions.

## Versions supportées

Android 5.0-11.0

##  Lancement du projet : 

Pour lancer le projet, il faut démarrer le mainActivity du package confroid ensuite démarrer le mainActivity de package testApp. 
Sur l'émulateur, on laisse l'application confroid en arrière plan. L'application testApp nous mettra en disposition des boutons. Choisir le type de configuration à afficher puis aller sur l'application confroid pour afficher en appuiyant sur le bouton configuration. 

## Limitations

Confroid _cible_ le SDK 29 (Android 10) et non le SDK 30 (Android 11); avec compileSdkVersion/targetSdkVersion à 30, les intents ne déclenchaient pas de services pour les appareils sous Android 11.
Malgré cela, l'application fonctionne toujours sous Android 11.

## Contenu du projet 

- Un package confroid representant l'application confroid
- Un package confroidUtils representant la bibliothèque utilitaire 
- Un package testApp pour pouvoir tester confroid 
- un dossier doc contenant un fichier AUTHORS, un rapport de developpement du projet et un manuel d'utilisation de l'application confroid.
- un dossier fastline contenant toutes les métadonnées de l'application confroid.
- Un fichier LICENSE
- ce readme 
