package fr.uge.confroid;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.uge.confroid.utils.ConfigToJson;
import fr.uge.confroid.utils.JsonToConfig;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ConfroidInstrumentedTest {

    @Test
    public void isByteConversionReversible() throws JSONException {
        byte[] expected = {0, 54, 45, 'f', 11, 1, 98, 45, -128, 88, 127};
        JSONArray jsonArray = new JSONArray(expected);
        byte[] actual = new byte[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {
            actual[i] = (byte) jsonArray.get(i);
        }
        
        assertArrayEquals(expected, actual);
    }
}