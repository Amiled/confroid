package fr.uge.confroid.services;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import fr.uge.confroid.BuildConfig;
import fr.uge.confroid.R;
import fr.uge.confroid.utils.ConfigToJson;
import fr.uge.confroid.database.Config;
import fr.uge.confroid.database.ConfigDao;
import fr.uge.confroid.database.ConfroidDatabase;

public class ConfigurationPusher extends IntentService {
    private static final String TAG = "ConfigurationPusher";

    private static final Object LOCK = new Object();

    private static final int PUSHER_NOTIFICATION_ID = 1;
    private static final String PUSHER_NOTIFICATION_CHANNEL_ID = BuildConfig.APPLICATION_ID + ".PUSHER_CHANNEL_ID";

    private static final String EXTRA_CONFIG = BuildConfig.APPLICATION_ID + ".services.extra.CONFIG";

    public ConfigurationPusher() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@NonNull Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            displayNotification();
        }

        new Thread(() -> {
            synchronized (LOCK) {
                Log.d(TAG, "Started service @ " + SystemClock.elapsedRealtime());
                Bundle config = intent.getBundleExtra(EXTRA_CONFIG);
                handleSaveConfig(config);
                Log.d(TAG, "Completed service @ " + SystemClock.elapsedRealtime());
            }
        }).start();
    }

    private void displayNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, PUSHER_NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.pusher_notification_text))
                    .setPriority(NotificationCompat.PRIORITY_LOW);

            startForeground(PUSHER_NOTIFICATION_ID, builder.build());
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.pusher_notification_channel_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(PUSHER_NOTIFICATION_CHANNEL_ID, name, importance);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void handleSaveConfig(Bundle config) {
        ConfroidDatabase db = ConfroidDatabase.getDB(getApplicationContext());
        ConfigDao dao = db.configDao();

        String name = config.getString("name");
        String packageName = name.split(" ")[0];
        String[] splitName = name.substring(packageName.length() + 1).split("/");
        String configName = splitName[0];
        String token = config.getString("token");
        String tag = config.getString("tag");
        long timestamp = config.getLong("timestamp");
        Object content = config.get("content");

        try {
            if (isTagOnlyRequest(tag, content)) {
                Log.d(TAG, "tag only");
                tagLatestVersion(dao, packageName, configName, token, tag);
                stopSelf();
            }

            if (splitName.length == 1) {
                Log.d(TAG, "full config");
                saveFullConfig(dao, packageName, configName, token, tag, timestamp, content);
            } else {
                Log.d(TAG, "partial config");
                String partial = splitName[1];
                savePartialConfig(dao, packageName, configName, partial, token, tag, timestamp, content);
            }
        } finally {
            db.close();
        }
    }

    private boolean isTagOnlyRequest(String tag, Object content) {
        return content == null && tag != null;
    }

    private void removeTag(ConfigDao dao, String packageName, String configName, String token, String tag) {
        Config config = dao.findTaggedConfig(packageName, configName, token, tag);
        if (config != null) {
            config.tag = null;
            dao.update(config);
        }
    }

    private void tagLatestVersion(ConfigDao dao, String packageName, String configName, String token, String tag) {
        Config lastConfig = dao.findLastConfig(packageName, configName, token);
        if (lastConfig != null) {
            removeTag(dao, packageName, configName, token, tag);
            lastConfig.tag = tag;
            dao.update(lastConfig);
        }
    }

    private void insertConfig(ConfigDao dao, String packageName, String configName, String token, String tag, int version, long timestamp, JSONObject json) {
        Config config;
        if (tag != null) {
            removeTag(dao, packageName, configName, token, tag);
            config = new Config(packageName, configName, token, version, tag, timestamp, json.toString());
        } else {
            config = new Config(packageName, configName, token, version, timestamp, json.toString());
        }
        Log.d(TAG, "inserted: " + config.toString());
        dao.insert(config);
    }

    private void saveFullConfig(ConfigDao dao, String packageName, String configName, String token, String tag, long timestamp, Object content) {
        Config precedentConfig = dao.findLastConfig(packageName, configName, token);
        int version;
        if (precedentConfig == null) {
            version = 0;
        } else {
            version = precedentConfig.version + 1;
        }
        JSONObject json = ConfigToJson.contentToJson(content);

        insertConfig(dao, packageName, configName, token, tag, version, timestamp, json);
    }

    private void savePartialConfig(ConfigDao dao, String packageName, String configName, String partial, String token, String tag, long timestamp, Object content) {
        String[] path = partial.split("\\.");
        JSONObject json = null;

        Config config = dao.findLastConfig(packageName, configName, token);
        if (config == null) {
            Log.e(TAG, "Caller app tried to edit non-existent configuration");
            stopSelf();
        }

        Object[] objects = getPartialObjects(path, config);

        try {
            if (content.getClass() == Bundle.class) {
                objects[objects.length - 1] = ConfigToJson.bundleToJson((Bundle) content);
            } else if (content.getClass() == byte[].class) {
                objects[objects.length - 1] = Arrays.toString((byte[]) content);
            } else {
                objects[objects.length - 1] = content;
            }
        } catch (JSONException e) {
            Log.e(TAG, "Caller app tried to push a non bundle or a non primitive");
            stopSelf();
        }

        try {
            JSONObject parent = (JSONObject) objects[objects.length - 2];
            Object child = objects[objects.length - 1];
            String key = path[path.length - 1];
            parent.put(key, child);

            json = new JSONObject();
            json.put("content", objects[0]);
        } catch (JSONException e) {
            Log.wtf(TAG, "repacking the json objects failed");
        }

        insertConfig(dao, packageName, configName, token, tag, config.version + 1, timestamp, json);
    }

    private Object[] getPartialObjects(String[] path, Config config) {
        Object[] objects = new JSONObject[1 + path.length];
        JSONObject base;
        try {
            base = new JSONObject(config.data);
            objects[0] = base.getJSONObject("content");
        } catch (JSONException e) {
            Log.wtf(TAG, "json data from the database is malformed");
        }

        for (int i = 1; i < objects.length; i++) {
            try {
                String key = path[i-1];
                objects[i] = ((JSONObject) objects[i-1]).get(key);
            } catch (JSONException e) {
                Log.e(TAG, "Caller app tried to edit non-existent configuration element");
                stopSelf();
            }
        }
        return objects;
    }
}