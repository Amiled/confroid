package fr.uge.confroid.services;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import fr.uge.confroid.BuildConfig;
import fr.uge.confroid.R;
import fr.uge.confroid.receivers.ConfigurationPuller;
import fr.uge.confroid.receivers.ConfigurationVersions;
import fr.uge.confroid.receivers.TokenDispenser;
import fr.uge.confroid.receivers.TokenRevoker;


public class MainService extends IntentService {
    private static final String TAG = "MainService";

    private static final int NOTIFICATION_ID = 2;
    private static final String NOTIFICATION_CHANNEL_ID = BuildConfig.APPLICATION_ID + ".SERVICE_CHANNEL_ID";

    private static final String ACTION_TOKEN_DISPENSER = BuildConfig.APPLICATION_ID + ".services.action.TOKEN_DISPENSER";
    private static final String ACTION_TOKEN_REVOKER = BuildConfig.APPLICATION_ID + ".services.action.TOKEN_REVOKER";
    private static final String ACTION_VERSIONS = BuildConfig.APPLICATION_ID + ".services.action.VERSIONS";
    private static final String ACTION_PULL_CONFIGURATION = BuildConfig.APPLICATION_ID + ".services.action.PULL_CONFIGURATION";

    public MainService() {
        super("MainService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            displayNotification();
        }
        new Thread(() -> {
            String action = intent.getAction();
            Context context = getApplicationContext();

            switch (action) {
                case ACTION_TOKEN_DISPENSER: TokenDispenser.dispenseToken(context, intent); break;
                case ACTION_TOKEN_REVOKER: TokenRevoker.revokeToken(context, intent); break;
                case ACTION_VERSIONS: ConfigurationVersions.sendVersions(context, intent); break;
                case ACTION_PULL_CONFIGURATION: ConfigurationPuller.pullConfig(context, intent); break;
                default: Log.d(TAG, "Unknown action: " + action);
            }
        }).start();
    }

    private void displayNotification() {
        createNotificationChannel();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.service_notification_text))
                .setPriority(NotificationCompat.PRIORITY_LOW);

        startForeground(NOTIFICATION_ID, builder.build());
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.service_notification_channel_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}