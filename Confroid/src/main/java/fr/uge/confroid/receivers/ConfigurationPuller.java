package fr.uge.confroid.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.core.content.ContextCompat;

import fr.uge.confroid.BuildConfig;
import fr.uge.confroid.database.Config;
import fr.uge.confroid.database.ConfigDao;
import fr.uge.confroid.database.ConfroidDatabase;
import fr.uge.confroid.utils.Consts;
import fr.uge.confroid.utils.JsonToConfig;

public class ConfigurationPuller {
    private static final String TAG = "ConfigurationPuller";

    private static final String EXTRA_CONFIG_NAME = BuildConfig.APPLICATION_ID + ".services.extra.CONFIG_NAME";
    private static final String EXTRA_RECEIVER = BuildConfig.APPLICATION_ID + ".services.extra.RECEIVER";
    private static final String EXTRA_TOKEN = BuildConfig.APPLICATION_ID + ".services.extra.TOKEN";
    private static final String EXTRA_REQUEST_ID = BuildConfig.APPLICATION_ID + ".services.extra.REQUEST_ID";
    private static final String EXTRA_TAG = BuildConfig.APPLICATION_ID + ".services.extra.TAG";

    public static void pullConfig(Context context, Intent intent) {
        String tmp = intent.getStringExtra(EXTRA_CONFIG_NAME);
        String packageName = tmp.split(" ")[0];
        String configName = tmp.substring(packageName.length() + 1);
        String token = intent.getStringExtra(EXTRA_TOKEN);
        long requestId = intent.getLongExtra(EXTRA_REQUEST_ID, -1);
        String tag = intent.getStringExtra(EXTRA_TAG);
        String receiver = intent.getStringExtra(EXTRA_RECEIVER);

        Log.d(TAG, requestId + "");
        Log.d(TAG, packageName);
        Log.d(TAG, configName);
        Log.d(TAG, token);
        Log.d(TAG, tag);

        Config config = getConfig(context, packageName, configName, token, tag);
        Log.d(TAG, "config: " + config);
        startService(context, packageName, configName, requestId, receiver, config);
    }

    private static void startService(Context context, String packageName, String configName, long requestId, String receiver, Config config) {
        final String EXTRA_REQUEST_ID = Consts.LIB_ID + ".services.extra.REQUEST_ID";
        final String EXTRA_CONFIG_NAME = Consts.LIB_ID + ".services.extra.CONFIG_NAME";
        final String EXTRA_VERSION = Consts.LIB_ID + ".services.extra.VERSION";
        final String EXTRA_CONTENT = Consts.LIB_ID + ".services.extra.CONTENT";
        final String EXTRA_CONTENT_TYPE = Consts.LIB_ID + ".services.extra.CONTENT_TYPE";
        final String ACTION_CONFIG_RECEIVER = Consts.LIB_ID + ".services.action.CONFIG_RECEIVER";

        Intent service = new Intent();
        service.setClassName(packageName, receiver);
        service.setAction(ACTION_CONFIG_RECEIVER);
        service.putExtra(EXTRA_REQUEST_ID, requestId);
        service.putExtra(EXTRA_CONFIG_NAME, configName);

        if (config != null) {
            Object content = JsonToConfig.jsonToContent(config.data);
            service.putExtra(EXTRA_VERSION, config.version);
            if (!tryPutPrimitive(service, EXTRA_CONTENT, EXTRA_CONTENT_TYPE, content)) {
                service.putExtra(EXTRA_CONTENT, (Bundle) content);
                service.putExtra(EXTRA_CONTENT_TYPE, "bundle");
            }
            Log.d(TAG, "content: " + content);
        }

        ContextCompat.startForegroundService(context, service);
    }

    private static Config getConfig(Context context, String packageName, String configName, String token, String tag) {
        ConfroidDatabase db = ConfroidDatabase.getDB(context);
        ConfigDao dao = db.configDao();

        try {
            if ("latest".equals(tag)) {
                return dao.findLastConfig(packageName, configName, token);
            }
            return dao.findTaggedConfig(packageName, configName, token, tag);
        } finally {
            db.close();
        }
    }

    public static boolean tryPutPrimitive(Intent service, String extraContent, String extraContentType, Object value) {
        Class<?> cls = value.getClass();
        if (cls.equals(String.class)) {
            service.putExtra(extraContent, (String) value);
            service.putExtra(extraContentType, "string");
            return true;
        } else if (cls.equals(byte[].class)) {
            service.putExtra(extraContent, (byte[]) value);
            service.putExtra(extraContentType, "bytes");
            return true;
        } else if (cls.equals(Float.class)) {
            service.putExtra(extraContent, (float) value);
            service.putExtra(extraContentType, "float");
            return true;
        } else if (cls.equals(Integer.class)) {
            service.putExtra(extraContent, (int) value);
            service.putExtra(extraContentType, "integer");
            return true;
        } else if (cls.equals(Boolean.class)) {
            service.putExtra(extraContent, (boolean) value);
            service.putExtra(extraContentType, "boolean");
            return true;
        }
        return false;
    }
}
