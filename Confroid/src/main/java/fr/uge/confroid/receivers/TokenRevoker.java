package fr.uge.confroid.receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.content.ContextCompat;

import fr.uge.confroid.BuildConfig;
import fr.uge.confroid.database.ConfroidDatabase;
import fr.uge.confroid.utils.Consts;

public class TokenRevoker {
    private static final String TAG = "TokenRevoker";

    private static final String EXTRA_RECEIVER = BuildConfig.APPLICATION_ID + ".services.extra.RECEIVER";
    private static final String EXTRA_PACKAGE_NAME = BuildConfig.APPLICATION_ID + ".services.extra.PACKAGE_NAME";
    private static final String EXTRA_TOKEN = BuildConfig.APPLICATION_ID + ".services.extra.TOKEN";

    public static void revokeToken(Context context, Intent intent) {
        String packageName = intent.getStringExtra(EXTRA_PACKAGE_NAME);
        String receiver = intent.getStringExtra(EXTRA_RECEIVER);
        String token = intent.getStringExtra(EXTRA_TOKEN);

        Log.d(TAG, "revoking " + packageName + " " + token);

        ConfroidDatabase db = ConfroidDatabase.getDB(context);
        db.configDao().revokeToken(packageName, token);
        db.close();

        startService(context, packageName, receiver, token);
    }

    private static void startService(Context context, String packageName, String receiver, String token) {
        final String EXTRA_TOKEN = Consts.LIB_ID + ".services.extra.TOKEN";
        final String ACTION_TOKEN_DELETER = Consts.LIB_ID + ".services.action.TOKEN_DELETER";

        Intent service = new Intent();
        service.setClassName(packageName, receiver);
        service.setAction(ACTION_TOKEN_DELETER);
        service.putExtra(EXTRA_TOKEN, token);
        ContextCompat.startForegroundService(context, service);
    }
}
