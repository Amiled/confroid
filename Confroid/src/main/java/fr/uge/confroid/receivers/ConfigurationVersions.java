package fr.uge.confroid.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.core.content.ContextCompat;

import java.util.List;

import fr.uge.confroid.BuildConfig;
import fr.uge.confroid.database.Config;
import fr.uge.confroid.database.ConfigDao;
import fr.uge.confroid.database.ConfroidDatabase;
import fr.uge.confroid.utils.Consts;

public class ConfigurationVersions {
    private static final String TAG = "ConfigurationVersions";

    private static final String EXTRA_RECEIVER = BuildConfig.APPLICATION_ID + ".services.extra.RECEIVER";
    private static final String EXTRA_TOKEN = BuildConfig.APPLICATION_ID + ".services.extra.TOKEN";
    private static final String EXTRA_CONFIG_NAME = BuildConfig.APPLICATION_ID + ".services.extra.CONFIG_NAME";
    private static final String EXTRA_REQUEST_ID = BuildConfig.APPLICATION_ID + ".services.extra.REQUEST_ID";

    public static void sendVersions(Context context, Intent intent) {
        String tmp = intent.getStringExtra(EXTRA_CONFIG_NAME);
        String packageName = tmp.split(" ")[0];
        String configName = tmp.substring(packageName.length() + 1);

        String token = intent.getStringExtra(EXTRA_TOKEN);
        long requestId = intent.getLongExtra(EXTRA_REQUEST_ID, -1);
        String receiver = intent.getStringExtra(EXTRA_RECEIVER);

        Log.d(TAG, packageName);
        Log.d(TAG, receiver);
        Log.d(TAG, token);

        Bundle versions = getVersions(context, configName, token);
        sendIntent(context, packageName, requestId, receiver, versions);
    }

    private static Bundle getVersions(Context context, String configName, String token) {
        ConfroidDatabase db = ConfroidDatabase.getDB(context);
        ConfigDao dao = db.configDao();
        List<Config> configs = dao.findConfigs(configName, token);
        Log.d(TAG, String.valueOf(configs.size()));
        Log.d(TAG, configName);
        Log.d(TAG, token);
        db.close();

        Bundle versions = new Bundle();
        for (Config c: configs) {
            versions.putBundle(Integer.toString(c.version), configToVersion(c));
        }
        return versions;
    }

    private static Bundle configToVersion(Config config) {
        Bundle version = new Bundle();
        version.putLong("timestamp", config.timestamp);
        if (config.tag != null) {
            version.putString("tag", config.tag);
        }
        return version;
    }

    private static void sendIntent(Context context, String packageName, long requestId, String receiver, Bundle versions) {
        String EXTRA_REQUEST_ID = Consts.LIB_ID + ".services.extra.REQUEST_ID";
        String EXTRA_VERSIONS = Consts.LIB_ID + ".services.extra.VERSIONS";
        String ACTION_VERSIONS_RECEIVER = Consts.LIB_ID + ".services.action.VERSIONS_RECEIVER";

        Intent intent = new Intent();
        intent.setClassName(packageName, receiver);
        intent.setAction(ACTION_VERSIONS_RECEIVER);
        intent.putExtra(EXTRA_REQUEST_ID, requestId);
        intent.putExtra(EXTRA_VERSIONS, versions);
        ContextCompat.startForegroundService(context, intent);
    }
}
