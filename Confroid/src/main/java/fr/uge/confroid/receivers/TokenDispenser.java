package fr.uge.confroid.receivers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.content.ContextCompat;

import java.util.Random;

import fr.uge.confroid.BuildConfig;
import fr.uge.confroid.database.ConfigDao;
import fr.uge.confroid.database.ConfroidDatabase;
import fr.uge.confroid.utils.Consts;

public class TokenDispenser {
    private static final String TAG = "TokenDispenser";

    private static final String EXTRA_RECEIVER = BuildConfig.APPLICATION_ID + ".services.extra.RECEIVER";
    private static final String EXTRA_PACKAGE_NAME = BuildConfig.APPLICATION_ID + ".services.extra.PACKAGE_NAME";

    private static final int TOKEN_LENGTH = 64;
    private static final char[] CHARS = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    public static void dispenseToken(Context context, Intent intent) {
        String receiver = intent.getStringExtra(EXTRA_RECEIVER);
        String packageName = intent.getStringExtra(EXTRA_PACKAGE_NAME);

        ConfroidDatabase db = ConfroidDatabase.getDB(context);
        ConfigDao dao = db.configDao();

        String token = generateToken();
        while (dao.usedToken(token)) { // Ensure every app uses a different token
            token = generateToken();
        }
        db.close();

        startService(context, packageName, receiver, token);
    }

    private static String generateToken() {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < TOKEN_LENGTH; i++) {
            int index = random.nextInt(CHARS.length);
            builder.append(CHARS[index]);
        }
        return builder.toString();
    }

    private static void startService(Context context, String packageName, String receiver, String token) {

        final String EXTRA_TOKEN = Consts.LIB_ID + ".services.extra.TOKEN";
        final String ACTION_TOKEN_RECEIVER = Consts.LIB_ID + ".services.action.TOKEN_RECEIVER";

        Log.d(TAG, EXTRA_TOKEN);
        Log.d(TAG, ACTION_TOKEN_RECEIVER);
        Log.d(TAG, token);
        Log.d(TAG, receiver);

        Intent intent = new Intent();
        intent.setClassName(packageName, receiver);
        intent.setAction(ACTION_TOKEN_RECEIVER);
        intent.putExtra(EXTRA_TOKEN, token);
        ContextCompat.startForegroundService(context, intent);
    }
}
