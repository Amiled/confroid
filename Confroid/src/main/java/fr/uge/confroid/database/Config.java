package fr.uge.confroid.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(primaryKeys = {"packageName", "configName", "token", "version"})
public class Config {
    @NonNull
    public String packageName;

    @NonNull
    public String configName;

    @NonNull
    public String token;

    @NonNull
    public int version;

    public String tag;

    public long timestamp;

    @NonNull
    public String data;

    public Config() {}

    @Ignore
    public Config(@NonNull String packageName, @NonNull String configName, @NonNull String token, int version, long timestamp, @NonNull String data) {
        this(packageName, configName, token, version, null, timestamp, data);
    }

    @Ignore
    public Config(@NonNull String packageName, @NonNull String configName, @NonNull String token, int version, String tag, long timestamp, @NonNull String data) {
        this.packageName = packageName;
        this.configName = configName;
        this.token = token;
        this.version = version;
        this.tag = tag;
        this.timestamp = timestamp;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Config{" +
                "packageName='" + packageName + '\'' +
                ", configName='" + configName + '\'' +
                ", token='" + token + '\'' +
                ", version=" + version +
                ", tag='" + tag + '\'' +
                ", timestamp=" + timestamp +
                ", data='" + data + '\'' +
                '}';
    }
}
