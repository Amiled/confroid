package fr.uge.confroid.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Config.class}, version = 1)
public abstract class ConfroidDatabase extends RoomDatabase {
    public abstract ConfigDao configDao();

    public static ConfroidDatabase getDB(Context context) {
        return Room.databaseBuilder(context, ConfroidDatabase.class, "confroid").build();
    }
}
