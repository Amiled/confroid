package fr.uge.confroid.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


@Dao
public interface ConfigDao {
    @Insert
    void insert(Config config);

    @Update
    void update(Config config);

    @Delete
    void delete(Config config);

    @Query("SELECT * FROM Config WHERE packageName == :packageName " +
            "AND configName == :configName AND token == :token ORDER BY version DESC LIMIT 1")
    Config findLastConfig(String packageName, String configName, String token);

    @Query("SELECT * FROM Config WHERE configName == :configName AND token == :token ORDER BY version ASC")
    List<Config> findConfigs(String configName, String token);

    @Query("SELECT * FROM Config ORDER BY timestamp DESC")
    List<Config> allConfig();

    @Query("SELECT * FROM Config WHERE tag == :tag AND " +
            "packageName == :packageName AND configName == :configName AND token == :token")
    Config findTaggedConfig(String packageName, String configName, String token, String tag);

    @Query("DELETE FROM Config WHERE packageName == :packageName AND token == :token")
    void revokeToken(String packageName, String token);

    @Query("SELECT EXISTS (SELECT * FROM Config WHERE token == :token)")
    boolean usedToken(String token);
}
