package fr.uge.confroid.utils;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

import fr.uge.confroid.R;
import fr.uge.confroid.database.Config;
import fr.uge.confroid.database.ConfigDao;
import fr.uge.confroid.database.ConfroidDatabase;

public class Backup {
    private static final String TAG = "Backup";

    public static final int CREATE_JSON_FILE = 1;
    public static final int PICK_JSON_FILE = 2;
    public static final String DEFAULT_FILE_NAME = "confroid.json";
    
    public static void writeBackup(Context context, Uri uri) throws IOException, JSONException {
        try (OutputStream stream = context.getContentResolver().openOutputStream(uri)) {
            stream.write(configData(context));
        }
    }

    private static byte[] configData(Context context) throws JSONException {
        ConfroidDatabase db = ConfroidDatabase.getDB(context);
        ConfigDao dao = db.configDao();
        List<Config> configs = dao.allConfig();
        db.close();

        JSONArray jsonArray = new JSONArray();
        for (Config config: configs) {
            jsonArray.put(configToJson(config));
        }
        try {
            return jsonArray.toString(2).getBytes();
        } catch (JSONException e) {
            Log.e(TAG, "Failed to prettify the json");
            throw e;
        }
    }

    private static JSONObject configToJson(Config config) throws JSONException {
        JSONObject json = new JSONObject();
        try {
            Object content = new JSONObject(config.data).get("content");
            json.put("package", config.packageName);
            json.put("config", config.configName);
            json.put("token", config.token);
            json.put("version", config.version);
            json.putOpt("tag", config.tag);
            json.put("timestamp", config.timestamp);
            json.put("content", content);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to create the JSON");
            throw e;
        }
        return json;
    }

    public static void loadBackup(Context context, Uri uri) throws JSONException, IOException {
        try (InputStream stream = context.getContentResolver().openInputStream(uri)) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringJoiner joiner = new StringJoiner("\n");

            String line;
            while ((line = reader.readLine()) != null) {
                joiner.add(line);
            }
            Log.d(TAG, joiner.toString());
            readConfigs(context, joiner.toString());
        }
    }

    private static void readConfigs(Context context, String string) throws JSONException {
        List<Config> configs =  new ArrayList<>();
        try {
            JSONArray array = new JSONArray(string);
            for (int i = 0; i < array.length(); i++) {
                JSONObject json = array.getJSONObject(i);
                configs.add(jsonToConfig(json));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Failed to parse the json");
            throw e;
        }
        Collections.reverse(configs);

        ConfroidDatabase db = ConfroidDatabase.getDB(context);
        ConfigDao dao = db.configDao();

        for (Config config: configs) {
            try {
                dao.insert(config);
            } catch (SQLiteConstraintException ignored) {
                // Config already in the database
            }
        }

        db.close();
    }

    private static Config jsonToConfig(JSONObject json) throws JSONException {
        JSONObject content = new JSONObject();
        content.put("content", json.get("content"));

        String tag;
        if (json.has("tag")) {
            tag = json.getString("tag");
        } else {
            tag = null;
        }
        return new Config(
            json.getString("package"),
            json.getString("config"),
            json.getString("token"),
            json.getInt("version"),
            tag,
            json.getLong("timestamp"),
            content.toString()
        );
    }
}
