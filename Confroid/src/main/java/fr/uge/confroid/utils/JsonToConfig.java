package fr.uge.confroid.utils;

import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class JsonToConfig {
    private static final String TAG = "JsonToConfig";

    public static Object jsonToContent(String string) {
        try {
            return jsonToContent(new JSONObject(string));
        } catch (JSONException e) {
            Log.wtf(TAG, "Content could not be retrieved from the json");
        }
        throw new AssertionError();
    }

    private static Object jsonToContent(JSONObject json) {
        Object content = null;
        try {
            content = json.get("content");
            Class<?> cls = content.getClass();

            if (cls.equals(JSONObject.class)) {
                return jsonToBundle((JSONObject) content);
            } else if (cls.equals(JSONArray.class)) {
                return jsonArrayToBytes((JSONArray) content);
            } else {
                return content;
            }
        } catch (JSONException e) {
            Log.wtf(TAG, "Content could not be retrieved from the json");
        }
        return content;
    }

    private static Bundle jsonToBundle(JSONObject json) throws JSONException {
        Bundle bundle = new Bundle();

        for (Iterator<String> it = json.keys(); it.hasNext(); ) {
            String key = it.next();
            Object object = json.get(key);
            Class<?> cls = object.getClass();

            if (cls.equals(JSONObject.class)) {
                bundle.putBundle(key, jsonToBundle((JSONObject) object));
            } else if (cls.equals(String.class)) {
                bundle.putString(key, json.getString(key));
            } else if (cls.equals(JSONArray.class)) {
                bundle.putByteArray(key, jsonArrayToBytes(json.getJSONArray(key)));
            } else if (cls.equals(Float.class)) {
                bundle.putFloat(key, (float) json.getDouble(key));
            } else if (cls.equals(Integer.class)) {
                bundle.putInt(key, json.getInt(key));
            } else if (cls.equals(Boolean.class)) {
                bundle.putBoolean(key, json.getBoolean(key));
            }
        }
        return bundle;
    }

    private static byte[] jsonArrayToBytes(JSONArray jsonArray) {
        byte[] array = new byte[jsonArray.length()];

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                int val = (int) jsonArray.get(i);
                array[i] = (byte) val;
            }
        } catch (JSONException e) {
            Log.wtf(TAG, "JSONArray -> byte[] conversion failed: someone messed with its contents");
        }

        return array;
    }
}
