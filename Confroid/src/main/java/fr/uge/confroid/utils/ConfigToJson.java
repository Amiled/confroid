package fr.uge.confroid.utils;

import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class ConfigToJson {
    private static final String TAG = "ConfigToJson";

    public static JSONObject contentToJson(Object content) {
        JSONObject json = new JSONObject();
        try {
            putItem(json, "content", content);
        } catch (JSONException e) {
            Log.wtf(TAG, "Content" + content.getClass().getCanonicalName() + "could not be transformed to JSON");
        }
        return json;
    }

    public static JSONObject bundleToJson(Bundle bundle) throws JSONException {
        JSONObject json = new JSONObject();

        for (String key: bundle.keySet()) {
            Object object = bundle.get(key);
            putItem(json, key, object);
        }
        return json;
    }

    private static void putItem(JSONObject json, String key, Object object) throws JSONException {
        Class<?> cls = object.getClass();

        if (cls.equals(Bundle.class)) {
            json.put(key, bundleToJson((Bundle) object));
        } else if (cls.equals(byte[].class)) {
            json.put(key, new JSONArray(object));
        } else {
            json.put(key, object);
        }
    }
}
