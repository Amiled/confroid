package fr.uge.confroid;

public interface ConfigClickItem {
    public void onItemClick(int position);
}
