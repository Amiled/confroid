package fr.uge.confroid;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.List;

import fr.uge.confroid.database.Config;

public class ConfItemAdaptater extends RecyclerView.Adapter<ConfItemAdaptater.ConfigItemHolder> {
    private final List<Config> arrayList;
    private final ConfigClickItem configClickItem;

    public ConfItemAdaptater(List<Config> objects, ConfigClickItem configClickItem) {
        this.arrayList=objects;
        this.configClickItem = configClickItem;
    }

    public class ConfigItemHolder extends RecyclerView.ViewHolder {
        private final TextView application;
        private final TextView version;
        private final TextView configName;
        private final TextView date;
        private final TextView token;

        public ConfigItemHolder(@NonNull View itemView) {
            super(itemView);
            application = itemView.findViewById(R.id.textView_application);
            configName = itemView.findViewById(R.id.textView_configName);
            version = itemView.findViewById(R.id.textView_display_token);
            date = itemView.findViewById(R.id.textView_date);
            token = itemView.findViewById(R.id.textView_token);

            itemView.setOnClickListener(v -> configClickItem.onItemClick(getAdapterPosition()));
        }

        @SuppressLint("SetTextI18n")
        public void maj(Config config){
            application.setText(config.packageName);
            configName.setText(config.configName);
            version.setText("Version " + config.version);
            date.setText(new Date(config.timestamp).toString());
            token.setText(config.token);
        }
    }

    @NonNull
    @Override
    public ConfItemAdaptater.ConfigItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new ConfigItemHolder(layoutInflater.inflate(R.layout.activity_config_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ConfItemAdaptater.ConfigItemHolder holder, int position) {
        holder.maj(arrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
