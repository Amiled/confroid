package fr.uge.confroid.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import fr.uge.confroid.ConfItemAdaptater;
import fr.uge.confroid.ConfigClickItem;
import fr.uge.confroid.R;
import fr.uge.confroid.database.Config;
import fr.uge.confroid.database.ConfigDao;
import fr.uge.confroid.database.ConfroidDatabase;

public class ConfigList extends AppCompatActivity implements ConfigClickItem {
    private static final String TAG = "ConfigList";

    private List<Config> configs;
    private ConfroidDatabase confroidDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_list);

        Thread thread = new Thread(() -> {
            confroidDatabase = ConfroidDatabase.getDB(this);
            ConfigDao configDao = confroidDatabase.configDao();
            configs = configDao.allConfig();
        });

        thread.start();
        try {
            thread.join();
        } catch (InterruptedException ignored) {
            Log.wtf(TAG, "interrupted while accessing the database");
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setAdapter(new ConfItemAdaptater(configs, this));
        LinearLayoutManager layoutManager = new LinearLayoutManager(ConfigList.this,LinearLayoutManager.VERTICAL,false);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(itemDecoration);
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, ConfigDisplay.class);
        Config config = configs.get(position);
        intent.putExtra("packageName", config.packageName);
        intent.putExtra("configName", config.configName);
        intent.putExtra("token", config.token);
        intent.putExtra("version", config.version);
        if (config.tag != null) {
            intent.putExtra("tag", config.tag);
        }
        intent.putExtra("data", config.data);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        confroidDatabase.close();
    }
}