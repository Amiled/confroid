package fr.uge.confroid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import fr.uge.confroid.R;

public class ConfigDisplay extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_display);

        TextView packageName = findViewById(R.id.textView_packageName);
        TextView configName = findViewById(R.id.textView_configName);
        TextView token = findViewById(R.id.textView_display_token);
        TextView version = findViewById(R.id.textView_version);
        TextView tag = findViewById(R.id.textView_tag);
        TextView data = findViewById(R.id.textView_data);

        Button button = findViewById(R.id.button_return);
        button.setOnClickListener(v -> finish());

        Intent intent = getIntent();
        packageName.setText(String.format(getString(R.string.package_name_format), intent.getStringExtra("packageName")));
        configName.setText(String.format(getString(R.string.config_name_format), intent.getStringExtra("configName")));
        token.setText(String.format(getString(R.string.token_format), intent.getStringExtra("token")));
        version.setText(String.format(getString(R.string.version_format), intent.getIntExtra("version", -1)));

        if (intent.hasExtra("tag")) {
            tag.setText(String.format(getString(R.string.tag_format), intent.getStringExtra("tag")));
        } else {
            tag.setVisibility(View.GONE);
        }
        data.setText(intent.getStringExtra("data"));
    }
}