package fr.uge.confroid.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;

import fr.uge.confroid.R;
import fr.uge.confroid.utils.Backup;


public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickConfigList(View v) {
        Intent intent = new Intent(this, ConfigList.class);
        startActivity(intent);
    }

    public void onClickExport(View v) {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/json");
        intent.putExtra(Intent.EXTRA_TITLE, Backup.DEFAULT_FILE_NAME);
        startActivityForResult(intent, Backup.CREATE_JSON_FILE);
    }

    public void onClickImport(View v) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/json");
        startActivityForResult(intent, Backup.PICK_JSON_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK || data == null) {
            return;
        }

        new Thread(() -> {
            Uri uri = data.getData();
            if (requestCode == Backup.CREATE_JSON_FILE) {
                tryWriteBackup(uri);
            } else if (requestCode == Backup.PICK_JSON_FILE) {
                tryLoadBackup(uri);
            }
        }).start();
    }

    private void tryLoadBackup(Uri uri) {
        try {
            Backup.loadBackup(this, uri);
            toast(R.string.file_success);
        } catch (FileNotFoundException e) {
            toast(R.string.file_error_moved);
        } catch (IOException e) {
            toast(R.string.file_error_read);
        } catch (JSONException e) {
            toast(R.string.file_error_invalid_json);
        }
    }

    private void tryWriteBackup(Uri uri) {
        try {
            Backup.writeBackup(this, uri);
            toast(R.string.file_success);
        } catch (FileNotFoundException e) {
            toast(R.string.file_error_moved);
        } catch (IOException e) {
            toast(R.string.file_error_write);
        } catch (JSONException e) {
            toast(R.string.file_error_invalid_json);
        }
    }

    private void toast(@StringRes int res) {
        runOnUiThread(() -> Toast.makeText(this, res, Toast.LENGTH_SHORT).show());
    }
}